import argparse
import dataclasses
import itertools
import json
import logging
import shutil
import time
import urllib

from datetime import datetime
from pathlib import Path
from typing import Optional, List

import tweepy
import youtube_dl


@dataclasses.dataclass
class TweetEntry:
    username: str
    author_id: int
    tweet_id: int
    created_at: datetime
    text: str
    tweet_url: List[str]
    media_type: Optional[str]
    media_urls: Optional[List[str]]


class TweetEntryEncoder(json.JSONEncoder):
    def default(self, o):
        if dataclasses.is_dataclass(o):
            return dataclasses.asdict(o)
        if isinstance(o, datetime):
            return {
                "_type": "datetime",
                "value": str(o),
            }
        return super().default(o)


class TweetEntryDecoder(json.JSONDecoder):
    def __init__(self, *args, **kwargs):
        super().__init__(object_hook=TweetEntryDecoder._object_hook, *args,
                         **kwargs)

    @staticmethod
    def _object_hook(obj):
        if "_type" not in obj:
            return obj
        _type = obj["_type"]
        if _type == "datetime":
            return datetime.fromisoformat(obj["value"])
        return obj


class IntRange:
    imin: Optional[int]
    imax: Optional[int]

    def __init__(self, imin=None, imax=None):
        self.imin = imin
        self.imax = imax

    def __call__(self, arg):
        try:
            value = int(arg)
        except ValueError as exc:
            raise self.exception() from exc
        if (
            (self.imin is not None and value < self.imin) or
            (self.imax is not None and value > self.imax)
        ):
            raise self.exception()
        return value

    def exception(self):
        msg = "Must be an integer"
        if self.imin is not None and self.imax is not None:
            msg = f"Must be an integer in the range [{self.imin}, {self.imax}]"
        elif self.imin is not None:
            msg = f"Must be an integer >= {self.imin}"
        elif self.imax is not None:
            msg = f"Must be an integer <= {self.imax}"
        return argparse.ArgumentTypeError(msg)


def main():
    opts = parse_args()
    logging.basicConfig(level=opts.loglevel)

    entries = fetch_existing_entries(opts.username,
                                     working_dir=opts.working_dir)

    if not opts.skip_scrape:
        client = tweepy.Client(opts.token)
        entries = fetch_liked_tweets(client, opts.username,
                                     max_results=opts.max_results,
                                     entries=entries)

    if entries:
        dump_tweets_metadata(opts.username, entries,
                             working_dir=opts.working_dir)
        dump_tweets_media(opts.username, entries,
                          working_dir=opts.working_dir)


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-t", "--token", action="store", type=str,
                        required=True)
    parser.add_argument("-u", "--username", action="store", type=str,
                        required=True)
    parser.add_argument("-m", "--max-results", action="store",
                        type=IntRange(5, 100), default=100)
    parser.add_argument("-s", "--skip-scrape", action="store_true")
    parser.add_argument("-w", "--working-dir", action="store", type=Path,
                        default=Path())
    parser.add_argument("-d", "--debug", action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)
    return parser.parse_args()


def fetch_existing_entries(username, working_dir=Path()):
    output_dir = working_dir.joinpath(username)
    metadata_file = output_dir.joinpath("metadata.json")
    if not metadata_file.exists():
        return []
    with open(str(metadata_file), "r", encoding="utf-8") as fstream:
        data = json.load(fstream, cls=TweetEntryDecoder)
        entries = [TweetEntry(**i) for i in data]
        return entries


def fetch_liked_tweets(client, username, max_results=100, entries=None):
    if not entries:
        entries = []

    existing = {i.tweet_id for i in entries}

    (
        tweets, users, medias, next_token
    ) = _fetch_liked_tweets(client, username, max_results=max_results)

    while tweets:
        for tweet in tweets:
            tweet_id = tweet.id
            if tweet_id in existing:
                return entries

            entry = process_tweet(tweet, users, medias)
            entries.append(entry)
            existing.add(tweet_id)
        time.sleep(5)
        (
            tweets, users, medias, next_token
        ) = _fetch_liked_tweets(client, username, max_results=max_results,
                                next_token=next_token)
    return entries


def _fetch_liked_tweets(client, username, max_results=100, next_token=""):
    logging.info("Fetching %s tweets...", max_results)
    res = client.get_user(username=username)
    kwargs = {
        "id": res.data.id,
        "media_fields": ["url"],
        "max_results": max_results,
        "tweet_fields": ["entities", "text", "created_at"],
        "expansions": ["author_id", "attachments.media_keys"],
    }

    if next_token:
        kwargs["pagination_token"] = next_token

    res = client.get_liked_tweets(**kwargs)

    if "next_token" in res.meta:
        next_token = res.meta["next_token"]

    tweets = res.data
    includes = res.includes
    users = {user.id: user for user in includes.get("users", {})}
    medias = {media.media_key: media for media in includes.get("media", {})}
    logging.info("Fetched %s tweets.", res.meta["result_count"])
    return tweets, users, medias, next_token


def process_tweet(tweet, users, medias):
    author_id = tweet.author_id
    username = users[author_id].username
    tweet_id = tweet.id
    created_at = tweet.created_at
    text = tweet.text
    tweet_url = process_tweet_url(tweet, username)
    media_type, media_urls = process_tweet_media(tweet, tweet_url, medias)
    entry = TweetEntry(username, author_id, tweet_id, created_at, text,
                       tweet_url, media_type, media_urls)
    return entry


def process_tweet_url(tweet, username):
    url = None
    if "entities" in tweet and "urls" in tweet["entities"]:
        urls = (i["expanded_url"] for i in tweet["entities"]["urls"])
        url = next((url for url in urls if "twitter.com" in url), None)
    if not url:
        url = f"https://twitter.com/{username}/status/{tweet.id}"
    return url


def process_tweet_media(tweet, tweet_url, medias):
    if not tweet.attachments:
        return None, None
    media_type = None
    media_urls = []
    media_keys = tweet.attachments["media_keys"]
    tweet_medias = [medias[key] for key in media_keys if key in medias]
    for media in tweet_medias:
        media_url = None
        if not media_type:
            media_type = media.type
        if media.type in ("video", "animated_gif"):
            media_url = extract_tweet_video_info(tweet_url)
        elif media.type == "photo":
            media_url = media.url
        if media_url:
            media_urls.append(media_url)
    return media_type, media_urls


def extract_tweet_video_info(tweet_url):
    logging.debug("Extracting video info for '%s'", tweet_url)
    ytdl_opts = {
        "format": "bestvideo[ext=mp4]+bestaudio[ext=m4a]/mp4",
        "quiet": True,
    }
    with youtube_dl.YoutubeDL(ytdl_opts) as ytdl:
        ytdl_res = ytdl.extract_info(tweet_url, download=False)
        logging.debug("youtube-dl response:\n%s", ytdl_res)
        if "requested_formats" in ytdl_res:
            return ytdl_res["requested_formats"][0]["url"]
        return ytdl_res["url"]


def dump_tweets_metadata(username, tweets, working_dir=Path()):
    output_dir = working_dir.joinpath(username)
    metadata_file = output_dir.joinpath("metadata.json")
    output_dir.mkdir(exist_ok=True)
    with open(str(metadata_file), "w", encoding="utf-8") as fstream:
        json.dump(tweets, fstream, cls=TweetEntryEncoder)


def dump_tweets_media(username, tweets, working_dir=Path()):
    output_dir = working_dir.joinpath(username)
    key = lambda x: x.author_id
    grouped_tweets = itertools.groupby(sorted(tweets, key=key), key)
    for author_id, group in grouped_tweets:
        working_dir = output_dir.joinpath(str(author_id))
        urls = [j for i in group if i.media_urls for j in i.media_urls]
        if not urls:
            continue
        working_dir.mkdir(exist_ok=True)
        for url in urls:
            fname = Path(urllib.parse.urlparse(url)[2]).name
            dest = working_dir.joinpath(fname)
            logging.info("Downloading '%s' to '%s'", url, str(dest))
            if dest.exists():
                logging.info("File already exists. Skipping...")
            else:
                download_file(url, str(dest))


def download_file(url, dest):
    with urllib.request.urlopen(url) as res, open(dest, "wb") as fstream:
        shutil.copyfileobj(res, fstream)


if __name__ == "__main__":
    main()
