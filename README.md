# dltweep

## Usage

```
$ python3 -m venv .venv
$ source .venv/bin/activate
$ pip3 install -r requirements.txt
$ python3 -m dltweep --help
$ python3 -m dltweep --token <Bearer Token> --username <username> --working-dir <path/to/folder>
```
