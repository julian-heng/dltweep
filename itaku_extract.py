#!/usr/bin/env python3


import argparse
import dataclasses
import json
import shutil
import subprocess
import sys
import urllib.parse

from datetime import date
from itertools import chain
from pathlib import Path
from typing import Callable, TypeVar, Iterable, Optional

from requests import Session
from requests.models import PreparedRequest


T = TypeVar("T")

def from_list(f: Callable[[any], T], x: any) -> list[T]:
    assert isinstance(x, list)
    return [f(y) for y in x]


def from_str(x: any) -> str:
    assert isinstance(x, str)
    return x


def from_int(x: any) -> int:
    assert isinstance(x, int) and not isinstance(x, bool)
    return x


class SzuruTag:
    names: list[str]

    def __init__(self, names: list[str]) -> None:
        self.names = names

    @staticmethod
    def from_dict(obj: any) -> 'SzuruTag':
        assert isinstance(obj, dict)
        names = from_list(from_str, obj.get("names"))
        return SzuruTag(names)


class SzuruPost:
    checksum_md5: str
    file_size: int
    content_url: str
    tags: list[SzuruTag]

    def __init__(self, checksum_md5: str, file_size: int, content_url: str, tags: list[SzuruTag]) -> None:
        self.checksum_md5 = checksum_md5
        self.file_size = file_size
        self.content_url = content_url
        self.tags = tags

    @staticmethod
    def from_dict(obj: any) -> 'SzuruPost':
        assert isinstance(obj, dict)
        checksum_md5 = from_str(obj.get("checksumMD5"))
        file_size = from_int(obj.get("fileSize"))
        content_url = from_str(obj.get("contentUrl"))
        tags = from_list(SzuruTag.from_dict, obj.get("tags"))
        return SzuruPost(checksum_md5, file_size, content_url, tags)


@dataclasses.dataclass
class Action:
    seq: int
    content_url: str
    md5: str
    tags: list[str]
    compress: Optional[int]


TagSet = dict[str, list[str]]


class SzuruSession(Session):
    def __init__(self, base_url=None):
        super().__init__()
        self.base_url = base_url
        self.headers.update({"Accept": "application/json"})

    def request(self, method, url, *args, **kwargs):
        joined_url = urllib.parse.urljoin(self.base_url, url)
        return super().request(method, joined_url, *args, **kwargs)


def main() -> None:
    opts = parse_args(sys.argv[1:])
    tag_set = get_tagset(opts.tag_set)
    with SzuruSession(opts.base_url) as session:
        session.auth = (opts.username, opts.password)
        posts = fetch_posts(session, opts.date_start, opts.date_end)
        actions = []
        for n, post in enumerate(posts, 1):
            tags = chain.from_iterable(tag_set.get(t, [t]) for t in chain.from_iterable(t.names for t in post.tags))
            action = Action(n, post.content_url, post.checksum_md5, tags, get_compress_quality(post.file_size))
            actions.append(action)

        opts.working_dir.mkdir(parents=True, exist_ok=True)
        with open(opts.working_dir.joinpath("tags.txt"), "w") as f:
            for action in actions:
                dest = (
                    opts.working_dir
                    .joinpath(f"{action.seq:02}")
                    .joinpath(f"{action.md5}{Path(action.content_url).suffix}")
                )
                url = urllib.parse.urljoin(opts.base_url, action.content_url)
                dest.parent.mkdir(exist_ok=True)
                download_file(url, dest)
                if action.compress:
                    subprocess.run(["mogrify", "-quality", str(action.compress), "-format", "jpg", dest])
                f.write(f"{action.seq:02}: {' '.join(action.tags)}\n")


def parse_args(args: list[str]) -> argparse.Namespace:
    parser = argparse.ArgumentParser()
    parser.add_argument("base_url", action="store", type=str)
    parser.add_argument("username", action="store", type=str)
    parser.add_argument("password", action="store", type=str)
    parser.add_argument("date_start", action="store", type=date.fromisoformat)
    parser.add_argument("date_end", action="store", type=date.fromisoformat)
    parser.add_argument("-t", "--tag-set", action="store", type=Path, default=None)
    parser.add_argument("-d", "--working-dir", action="store", type=Path, default=Path("."))

    return parser.parse_args(args)


def get_tagset(filepath: Path) -> TagSet:
    if not filepath:
        return {}
    with open(filepath, "r") as f:
        return json.load(f)


def get_compress_quality(num_bytes: int) -> int:
    if num_bytes <= 6e+6:
        return None
    elif num_bytes <= 20e+6:
        return 95
    else:
        return 90


def fetch_posts(session: SzuruSession, date_start: "datetime", date_end: "datetime") -> Iterable[SzuruPost]:
    params = {
        "offset": 0,
        "limit": 100,
        "query": f"creation-date:{date_start:%Y-%m-%d}..{date_end:%Y-%m-%d} "
        f"sort:creation-date source:*tumblr*"
    }
    res = session.get("/api/posts", params=params)
    yield from [] if res.status_code != 200 else map(SzuruPost.from_dict, res.json()["results"][::-1])


def download_file(url: str, dest: Path) -> None:
    with urllib.request.urlopen(url) as res, open(dest, "wb") as fstream:
        shutil.copyfileobj(res, fstream)


if __name__ == "__main__":
    main()
